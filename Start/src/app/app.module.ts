import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { CommentsComponent } from './comments/comments.component';
import { RouterModule} from '@angular/router';
import { PlanComponent } from './plan/plan.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    CommentsComponent,
    PlanComponent,
    PortfolioComponent,
    HomeComponent

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent },
      { path: 'plan', component: PlanComponent },
      { path: 'portfolio', component: PortfolioComponent },
      { path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: '*', redirectTo: 'home', pathMatch: 'full'}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
